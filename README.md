# Data and scripts for manuscript "Impact of biopower generation on eastern US forests"

**REPOSITORY CITATION**

Mirzaee, Ashkan. Data and scripts for manuscript "Impact of biopower generation on eastern US forests" (2021). https://doi.org/10.6084/m9.figshare.14738277

**RELATED PUBLICATION**

Mirzaee, A., McGarvey, R.G., Aguilar, F.X. et al. Impact of biopower generation on eastern US forests. Environ Dev Sustain (2022). https://doi.org/10.1007/s10668-022-02235-4

## Access conditions
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
Sourcecode is available under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contents
Bash and Python scripts to provide input data for a statistical learning model written in R to generate manuscript results and graphics.

## Contact information
- Ashkan Mirzaee: amirzaee@mail.missouri.edu

## System requirement
The workflow in this repository is designed to run in both parallel and serial. To run this application in parallel you need a Linux based cluster with [slurm](https://slurm.schedmd.com/) job scheduling system. As a student, faculty or researcher, you might have access to your institute's cluster by using `ssh username@server.domain` on a Unix Shell (default terminal on Linux and macOS computers) or an SSH Client. If you do not have access to a cluster, the workflow can be run in serial on personal computers' Unix Shell. A basic knowledge about [Unix Shell](https://ashki23.github.io/shell.html) and [HPC](https://ashki23.github.io/hpc.html) can help to follow the workflow easily.

**Note:** this application will collect and trim input data from different resources. You can also find all input data in the [backup repository](https://gitlab.com/ashki23/backup-biopower).

## Software
The following software and packages are required:

- Miniconda3
- Python > 3.8
  - xlrd
  - openpyxl
  - shapely
  - fiona
  - jq
- R > 4.0
  - sf = 0.9_6
  - lwgeom = 0.1_7
  - nlme
  - rjson
  - knitr
  - ggplot2

`environment.sh` is designed to install the required software.

## Configurations
In this project we focused on changes among selected attributes in the Eastern US for 2005, 2008, 2011, 2014 and 2017. The following shows the project configurations:
```json
{
    "year" : [2005,2008,2011,2014,2017],
    "state": ["AL","AR","CT","DE","FL","GA","IA","IL","IN","KY","LA","ME","MA","MD","MI","MN","MO","MS","NC","NH","NJ","NY","OH","PA","RI","SC","TN","VA","VT","WI","WV"],
    "attribute_cd": [7,9,69,61],
    "tolerance": 1,
    "power_plant_cap_mwh": 2000000,
    "pellet_plant_min_ton": 10000,
    "power_plant_radius_mile": [30,50],
    "pellet_plant_radius_mile": 50,
    "other_radius_mile": 75,
    "job_number_max": 24,
    "job_time_hr": 8,
    "partition": "Lewis"
}
```

**Metadata** - `config.json` includes:

- **year:** list of years. For a single year use a singleton list *e.g. [2017]*
- **state:** list of states. Use `["ALL"]` to include all states in the data. For a single state use a singleton list *e.g. ["MO"]*
- **attribute_cd:** list of FIA forest attributes code. For a single code use a singleton list *e.g. [7]*
- **tolerance:** a binary variable of 0 or 1. Set 1 to use the closest available [FIA survey year](https://apps.fs.usda.gov/fia/datamart/recent_load_history.html) to the listed years, if the listed year is not available in FIA
- **pellet_plant_cap_ton:** a threshold (tons) to indicate large capacity pellet plants
- **power_plant_cap_mwh:** a minimum threshold of annual MWh generation of wood-using power plants
- **pellet_plant_radius_mile:** list of radii (miles) for small and large procurement areas of pellet mills. For a single radius use a singleton list *e.g. [50]*
- **power_plant_radius_mile:** radius (miles) for procurement area of power plants
- **other_radius_mile:** radius (miles) for procurement area of pulp mills and a threshold (miles) to access ports
- **job_number_max:** max number of jobs that can be submitted at the same time. Use 1 for running in serial
- **job_time_hr:** estimated hours that each job might takes (equivalent to `--time` slurm option). Not required to change for running in serial
- **partition:** name of the selected partition in the cluster (equivalent to `--partition` slurm option). Not required to change for running in serial

**Notes:**
- For serial running in personal computers set `"job_number_max": 1`. Otherwise, increasing number of jobs can submit more parallel jobs and reduce running time
- Selected years should have a same time laps. For instance, every year or every two years and etc. (here we have a 3-year window) 
- EIA and FIA data are mostly available from year 2002, therefore, minimum year minus length of the year window should be greater than 2002
- Statistical analaysis needs at least three years of observations and enough locations
- Change values in the `config.json` to run the application for different attributes, thresholds, years and locations

### Wood and coal burning power plants
Data about wood and coal using generators and power plants are downloaded from:
- [EIA-860](https://www.eia.gov/electricity/data/eia860/) - includes information about generators and power plants
- [EIA-923](https://www.eia.gov/electricity/data/eia923/) - includes annual MWh generation of power plants by fuel

We used EIA-860 *"Generator_Yr.xls"* to select power plants using woody biomass and coal fuels (**Table 1**); and EIA-860 *"Plant_Yr.xls"* to find the power plants location. We also, used *"Generation and Fuel Data"* from EIA-923 to find total bioenegy generation of selected power plants.

**Table 1**: Woody biomass and coal fules

Type|Code|Unit|Lower Higher Heating Value (MMBtu)|Upper Higher Heating Value (MMBtu)|Description
---|---|---|---|---|---
Biomass|BLQ|tons|10|14|Black Liquor
Biomass|WDS|tons|7|18|Wood/Wood Waste Solids (incl. paper pellets, railroad ties, utility poles, wood chips, bark, and wood waste solids)
Biomass|WDL|barrels|8|14|Wood Waste Liquids excluding Black Liquor (including red liquor, sludge wood, spent sulfite liquor, and other wood-based liquids)
Coal|ANT|Tons|22|28|Anthracite Coal 
Coal|BIT|Tons|20|29|Bituminous Coal
Coal|LIG|Tons|10|14.5|Lignite Coal
Coal|SGC|Mcf|0.2|0.3|Coal-Derived Synthesis Gas
Coal|SUB|Tons|15|20|Subbituminous Coal
Coal|WC|tons|6.5|16|Waste/Other Coal (incl. anthracite culm, bituminous gob, fine coal, lignite waste, waste coal)
Coal|RC|tons|20|29|Refined Coal

**Metadata** - `power_info_bimass.py` and `power_info_coal.py` generate CSV files including:

- **plant_code:** EIA power plant code
- **state:** power plants state
- **lat:** power plants latitude
- **lon:** power plants longitude
- **nerc_region:** North American Electric Reliability Corporation ([NERC](https://www.nerc.com/AboutNERC/keyplayers/Pages/default.aspx)) regions
- **opened:** year that the first biomass generaror started operating at each power plant
- **retired:** year that all biomass generaror were retired at each power plant
- **total_generation:** total bioenergy generation (MWh) at each power plant from 2003 to 2017; 0 for coal-firing plants
- **radius:** radius of the power plant procurement area
- **year:** year of the EIA data
- **net_generation:** amount of bioenergy generation (MWh) at each power plant at each year i.e. 2005, 2008, 2011, 2014, 2017; 0 for coal-firing plants
- **net_period_generation:** total bioenergy generation (MWh) at each power plant in 3-year periods i.e. 2003-2005, 2006-2008, ..., 2015-2017; 0 for coal-firing plants

### Wood pellet plants and pulp mill facilities
Pellet data is collected from [rc-biomass](https://gitlab.com/ashki23/rc-biomass) project. We only used hardwoods and softwoods pellet mills with more than 10 thousand annual capacity located in the Eastern US.

**Metadata** - `pellet_info.csv` includes:

- **name:** name of the wood pellet manufacturer
- **feedstock:** feedstock typr
- **capacity:*** anuual capacity (tons)
- **city:** pellet plant city
- **state:*** pellet plant state
- **lat:*** pellet plant latitude
- **lon:*** pellet plant longitude
- **status:** binary variable, Operational/Other
- **opened:*** year that pellet plant is opened
- **retired:*** year that pellet plant is retired or current year if operational

**Note:** you can use your own pellet plant dataset. The data should be a CSV file named `pellet_info.csv` and located in the main directory (`HOME_PROJ`). Your data can include different columns but marked (*) columns are required and should have the same header.

**Pulp mills** dataset extracted from annual reports from US Forest Service:

- [U.S. Forest Service Southern Pulpwood Production, from 2000 to 2016](https://www.fs.usda.gov/treesearch/search?keywords=%22Southern+pulpwood+production%22&authorlname=&yearfrom=&yearto=&station=&series=&volume=) - Table A.21 or A.22 for old versions and A.8 for recent years
- [U.S. Forest Service National Pulpwood Production, 2008 - 2010](https://www.fs.usda.gov/treesearch/search?keywords=%22National+pulpwood+production%22&authorlname=&yearfrom=&yearto=&station=&series=&volume=) - Table A.31 (2010) and table A.29 (2008)
- [U.S. Wood-Using Mill Locations, 2005](https://www.srs.fs.usda.gov/econ/data/mills/)

**Metadata** - pulp mills dataset (`pulp_info.csv`) includes:

- **company:** name of the pulp mill company
- **city:** pulp mill city
- **state:*** pulp mill state
- **year:*** year that pulp mill is retired or latest report year if operational
- **lat:*** pulp mill latitude
- **lon:*** pulp mill longitude

### NERC regions, forest regions, ports, counties' population and crop acreage
Geo data related to North American Electric Reliability Corporation (NERC), forest regions, ports, counties and states shapefiles and also counties population and crop acreage information are downloaded through `download.sh` from:

- [NERC regions](https://www.eia.gov/maps/map_data/NERC_Regions_EIA.zip) from [EIA Mape](https://www.eia.gov/maps/layer_info-m.php)
- [Forest regions](https://people.ohio.edu/dyer/dyer_forestregions.zip) from [Prof. James Dyer, Ohio University](https://people.ohio.edu/dyer/forest_regions.html) 
- [Major ports](https://opendata.arcgis.com/datasets/f682e9a104884a9385630ab846cbf873_0.zip) from [US Department of Transportation](https://data-usdot.opendata.arcgis.com/datasets/major-ports) - [**Metadata**](https://data-usdot.opendata.arcgis.com/datasets/major-ports)
- [Counties shapefile](https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_20m.zip) from [US Census](https://www.census.gov/geographies/mapping-files/time-series/geo/carto-boundary-file.html)
- [States shapefile](https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_state_20m.zip) from [US Census](https://www.census.gov/geographies/mapping-files/time-series/geo/carto-boundary-file.html)
- [Counties population estimation 2010-2018](https://www2.census.gov/programs-surveys/popest/datasets/2010-2018/counties/totals/co-est2018-alldata.csv) from [US Census Surveys 2010-2018](https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-total.html) - [**Metadata**](https://www2.census.gov/programs-surveys/popest/technical-documentation/file-layouts/2010-2018/co-est2018-alldata.pdf)
- [Counties population estimation 2000-2009](https://www2.census.gov/programs-surveys/popest/datasets/2000-2009/counties/totals/co-est2009-alldata.csv) from US Census Surveys 2000-2009
- [Counties crop acreage data 2007-2020](https://www.fsa.usda.gov/news-room/efoia/electronic-reading-room/frequently-requested-information/crop-acreage-data/index) from US Department of Agriculture. These information are collected in `crop_acerage.csv` file

**Metadata** - `population_info.csv` includes counties population for the selected years (generated by `prep_data.py`):

- **state:** Census state code
- **county:** Census county code
- **stname:** state name
- **ctyname:** county name
- **popestimate2005:** county population in 2005
- **popestimate2008:** county population in 2008
- **popestimate2011:** county population in 2011
- **popestimate2014:** county population in 2014
- **popestimate2017:** county population in 2017

**Metadata** - `crop_acreage.csv` includes counties crop acreage since 2007:

- **State_Code:** Census state code 
- **County_Code:** Census county code
- **State_County_Code:** Concatenated Census state and county code
- **State:** state name
- **County:** county name
- **Year:** year
- **Planted_Acres:** total planted area per acre

**Notes:** 
- Information for counties planted acres in `crop_acreage.csv` for years 2007 and 2008 are estimated based on 2009 county level informtion and state level data for years 2007 and 2008
- If major ports's link is not working, find the download link in [here](https://data-usdot.opendata.arcgis.com/datasets/major-ports) and update the link in `download.sh`

### Drought dataset
Drought GIS information is downloaded through `usdm_download.py` from:
- [US Drough Monitor GIS data](https://droughtmonitor.unl.edu/Data/GISData.aspx) - [**Metadata**](https://droughtmonitor.unl.edu/Data/Metadata.aspx)

Extreme drought was included as a one-year lagged variable to correctly infer associations due to recording of drought in the summer months and surveying of forest plots throughout the year.

### FIA database
Power data is prepared (`prep_data.py`) by adding the following variables to `power_info.csv`:

- **unit_id:** power plant unique number
- **state_cd:** FIA state code
- **neighbors:** list of neighbouring states
- **neighbors_cd:** list of FIA codes of neighbouring states
- **radius:** radius (miles) of the power plant procurement area

`prep_data.py` is generating a JSON output to feed `fia_coordinate.py`. We used this Python scraping program to collect the forest attributes within the power plants' procurement area for the five time intervals from [FIA EVALIDator](https://apps.fs.usda.gov/Evalidator/evalidator.jsp). We added all neighbouring states into the searching areas to make sure level of forest attributes within circular procurement areas whose located in multi-state areas are captured completely. Forest Inventory and Analysis (FIA) data is downloaded for the Eastern US states in about tens of parallel jobs to minimize downloading time. In this study, we collected level of the following timberland attributes: 

- **69:** above and belowground carbon in live trees (tons)
- **61:** above and belowground carbon in standing-dead trees (tons)
- **66:** carbon in organic soil (tons)

`fia_coordinate.py` generates a CSV file in a panel format and a JSON file including power plants information and level of forest attributes within their procurement areas for the selected years.

**Metadata** - the Forest Inventory and Analysis (FIA) description and user guide (version 8.0) is available [here](https://www.fia.fs.fed.us/library/database-documentation/current/ver80/FIADB%20User%20Guide%20P2_8-0.pdf). States codes and FIA attributes are listed at B and O appendices respectively.

Moreover, we collected level of the above attributes for all the studied states by `fia_county.py` to compare attribute changes in states with the changes in the procurement areas over time.

**Note:** FIA EVALIDator might not be available during maintaining and updating FIA databases. Please check [FIA User Alerts](https://www.fia.fs.fed.us/tools-data/) for more information.

## Data analysis
Above data is used in `panel_regression.R` to find the following explanatory variables for each analysis area in the study and run the statistical model:

- **est_net_period_generation:** Estimated bioenergy generation per GWh the analysis area within a three-year period starting year 2003
- **years_operation:** Average years of operation of power plants whose procurement zone intersects with the analysis area since 1990
- **n.overlaps** Number of power plants procurement or control zones contained in the analysis area
- **pellet_intersection:** Total percentage of the analysis area covered by wood pellet plantsprocurement zones
- **pulp_intersection:** Total percentage of the analysis area covered by pulp mills procurement zones
- **drought:** Dichotomous variable; 1 if severe, extreme, or exceptional drought reported for in at least 5% of the analysis area in August preceding a given year, 0 otherwise
- **drought_intersection:** Total percentage of the analysis area impacted by severe, extreme, or exceptional drought reported in August preceding a given year
- **forest:** Categorical variable including eight ecological regions with Mesophytic as baseline; largest forest region within the analysis area
- **population:** Estimated population per per $`km^2`$ within the analysis area
- **cropland_ratio:** Estimated ratio of cropland areas within the analysis area
- **port_access:** Dichotomous variable; 1 if centroid of the analysis area was located within 100 $`km`$ of a major port exporting forest products, 0 otherwise 

## Workflow
The workflow includes the following steps:

- Setup environments
- Download datasets by Bash and Python
- Data preparation by Python
- Download FIA dataset by Python
- Data analysis by R

You may can the workflow in `batch_file.sh`. To submit all jobs and generate outputs, run `sbatch batch_file.sh` in a cluster or `source batch_file.sh` in a Unix Shell.

---
<div align="center">
Copyright 2021-2022, [Ashkan Mirzaee](https://ashki23.github.io/index.html) | Content is available under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) | Sourcecode licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
</div>

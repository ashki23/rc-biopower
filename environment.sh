#!/bin/bash

export PROJ_HOME=${PWD}
export OUTPUT=${PWD}/outputs
export OTHER=${PWD}/other_data
export FIA=${PWD}/fia_data
install -dvp ${OUTPUT}
install -dvp ${OTHER}/power_plants
install -dvp ${FIA}/survey
MYHOME=${HOME}

## Install Miniconda
if [ ! -d miniconda ]; then 
source bootstrap.sh
fi

## Initiate conda
export HOME=${PROJ_HOME}
export PATH=${PROJ_HOME}/miniconda/bin/:${PATH}
source ./miniconda/etc/profile.d/conda.sh

## Deactivat active envs
conda deactivate

echo ============ Create Conda envs ============= $(hostname) $(date) 

## Create local environments
if [ ! -d power_py_env ]; then
## Including: python openpyxl xlrd jq shapely fiona
conda create --yes --prefix ./power_py_env --file ./power_py_env.txt
fi

if [ ! -d power_r_env ]; then
## Including: r-base r-nlme r-sf=0.9_6 r-lwgeom=0.1_7 r-rjson r-knitr r-ggplot2
conda create --yes --prefix ./power_r_env --channel conda-forge --file ./power_r_env.txt
## lwgeom 0.1_7 needs libproj.so.15
ln -s ${PROJ_HOME}/power_r_env/lib/libproj.so ${PROJ_HOME}/power_r_env/lib/libproj.so.15
fi

## Activate Python environment
conda activate ./power_py_env
export HOME=${MYHOME}
sleep 3
